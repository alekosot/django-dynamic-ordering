from django.conf.urls import url
from .views import dynamic_ordering


app_name = 'dynamic_ordering'
urlpatterns = [
    url(r'(?P<content_type_pk>[\w]+)/'
        r'(?P<order_by_field>[\w]+)/'
        r'(?P<primary_keys>[\w,]+)/',
        dynamic_ordering,
        name='dynamic_ordering'),
]
