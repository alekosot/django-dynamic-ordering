from django import template
from django.template.loader import get_template

from dynamic_ordering import settings as conf
from dynamic_ordering.utils import dynamic_ordering_url


register = template.Library()

register.simple_tag(dynamic_ordering_url)


@register.simple_tag
def dynamic_ordering_partial_template(model):
    app_label = model._meta.app_label
    model_name = model._meta.model_name
    tpl = '{}{}/{}/partials/dynamic_ordering.html'.format(
        conf.TEMPLATE_PREFIX, app_label, model_name
    )
    try:
        get_template(tpl)
    except template.TemplateDoesNotExist:
        tpl = 'dynamic_ordering/partials/dynamic_ordering.html'

    return tpl
