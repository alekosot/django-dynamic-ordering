from django.apps import AppConfig


class DynamicOrderingAppConfig(AppConfig):
    name = 'dynamic_ordering'
