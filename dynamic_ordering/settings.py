from django.conf import settings
from .utils import sanity_check, security_check


BASE_TEMPLATE = getattr(
    settings, 'DYNAMIC_ORDERING_BASE_TEMPLATE',
    'dynamic_ordering/dynamic_ordering_base.html'
)

TEMPLATE_PREFIX = getattr(
    settings, 'DYNAMIC_ORDERING_TEMPLATE_PREFIX', ''
)

CHECKS = getattr(
    settings, 'DYNAMIC_ORDERING_CHECKS', (security_check, sanity_check)
)
