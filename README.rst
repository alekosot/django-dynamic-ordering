=======================
Django Dynamic Ordering
=======================

Ordering of models, based on configurable sort order field and customizable
templates


Dependencies
============

- `django.contrib.contenttypes`
