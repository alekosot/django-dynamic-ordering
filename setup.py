#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

from dynamic_ordering import __author__, __email__, __version__

with open('README.rst') as readme_file:
    readme = readme_file.read()


requirements = [
    'django',
]

test_requirements = [
]

setup(
    name='django_dynamic_ordering',
    version=__version__,
    description="Extensions on django-oscar for the needs of Radial",
    long_description=readme,
    author=__author__,
    author_email=__email__,
    url='https://gitlab.com/alekosot/django-dynamic-ordering',
    packages=[
        'django-dynamic-ordering',
    ],
    package_dir={
        'django-dynamic-ordering': 'dynamic_ordering'
    },
    include_package_data=True,
    install_requires=requirements,
    license="BSD license",
    zip_safe=False,
    keywords='django_stentor',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
